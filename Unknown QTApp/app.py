import sys
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt, QTimer
from pygame import mixer
import glob
import pickle
import datetime
import random
from mutagen.mp3 import MP3

class Player(QWidget):
    def __init__(self):
        super().__init__()
        try:
            with open ('musicFiles', 'rb') as file:
                self.musicFiles = pickle.load(file)
        except:
            self.musicFiles = []

        self.mixer = mixer
        self.initUI()
        self.paused = False
        self.songPositionSecs = 0
        self.timer = QTimer(self.window)
        self.timer.timeout.connect(self.updatePositionLabel)
        self.positionSliderGrabbed = False
        self.currentSongLength = 0

    def initUI(self):
        self.songFrequency = '47500'
        self.songVolume = '10'
        self.window = QMainWindow()
        self.window.setWindowTitle("i look like shit please run me on GNU/Linux with a gtk theme")
        self.window.setGeometry(600, 60, 1000, 900)

        #MenuBar
        fileAction = QAction("Load music file", self)
        fileAction.setShortcut("Ctrl+O")
        fileAction.triggered.connect(self.getfile)

        folderAction = QAction("Load all music in a folder", self)
        folderAction.setShortcut("Ctrl+Shift+O")
        folderAction.triggered.connect(self.getFolder)

        emptyAction = QAction("Clear all music from the playlist", self)
        emptyAction.setShortcut("Ctrl+Shift+C")
        emptyAction.triggered.connect(self.emptyQueue)

        exitAction = QAction("Exit", self)
        exitAction.setShortcut("Ctrl+Q")
        exitAction.triggered.connect(sys.exit)

        mainMenu = self.window.menuBar()
        fileMenu = mainMenu.addMenu('&File')
        fileMenu.addAction(fileAction)
        fileMenu.addAction(folderAction)
        fileMenu.addAction(emptyAction)
        fileMenu.addAction(exitAction)

        #Play Button
        self.playButton = QPushButton("PLAY", self.window)
        self.playButton.setGeometry(100,200, 800, 100)
        f = QFont("Deja Vu", 70, QFont.Bold)
        self.playButton.setFont(f)
        self.playButton.clicked.connect(self.play)

        #Pause/Resume Button
        self.pauseButton = QPushButton("PAUSE", self.window)
        self.pauseButton.setGeometry(100,315, 800, 100)
        f = QFont("Deja Vu", 70, QFont.Bold)
        self.pauseButton.setFont(f)
        self.pauseButton.clicked.connect(self.pause)

        #Previous Button
        self.nextButton = QPushButton("PREV", self.window)
        self.nextButton.setGeometry(100,430, 175, 75)
        f = QFont("Deja Vu", 40, QFont.Bold)
        self.nextButton.setFont(f)
        self.nextButton.clicked.connect(lambda: self.play("previous"))

        #Shuffle Button
        self.nextButton = QPushButton("SHUFFLE", self.window)
        self.nextButton.setGeometry(300,430, 400, 75)
        f = QFont("Deja Vu", 40, QFont.Bold)
        self.nextButton.setFont(f)
        self.nextButton.clicked.connect(self.shuffle)

        #Next Button
        self.nextButton = QPushButton("NEXT", self.window)
        self.nextButton.setGeometry(725,430, 175, 75)
        f = QFont("Deja Vu", 40, QFont.Bold)
        self.nextButton.setFont(f)
        self.nextButton.clicked.connect(lambda: self.play("next"))

        #Frequency text label
        self.labelFrequency = QLabel("Frequency", self.window)
        self.labelFrequency.setGeometry(100, 40, 70, 30)

        #music frequency slider
        self.sliderFrequency = QSlider(Qt.Horizontal, self.window)
        self.sliderFrequency.setRange(10000, 100000)
        self.sliderFrequency.setSliderPosition(int(self.songFrequency))
        self.sliderFrequency.setFocusPolicy(Qt.NoFocus)
        self.sliderFrequency.setGeometry(175, 40, 100, 30)
        self.sliderFrequency.valueChanged.connect(self.updateFrequency)

        #frequency value label
        self.labelFrequencyValue = QLabel(self.songFrequency, self.window)
        self.labelFrequencyValue.setGeometry(280, 40, 50, 30)

        #Volume text label
        self.labelVolume = QLabel("Volume", self.window)
        self.labelVolume.setGeometry(100, 60, 70, 30)

        #music volume slider
        self.sliderVolume = QSlider(Qt.Horizontal, self.window)
        self.sliderVolume.setFocusPolicy(Qt.NoFocus)
        self.sliderVolume.setRange(0, 10)
        self.sliderVolume.setSliderPosition(int(self.songVolume))
        self.sliderVolume.setGeometry(175, 60, 100, 30)
        self.sliderVolume.valueChanged.connect(self.updateVolume)

        #volume value label
        self.labelVolumeValue = QLabel(self.songVolume, self.window)
        self.labelVolumeValue.setGeometry(280, 60, 50, 30)

        #Song Position text label
        self.labelPosition = QLabel("0:00:00", self.window)
        self.labelPosition.setGeometry(100, 170, 50, 30)

        #music volume slider
        self.sliderPosition = QSlider(Qt.Horizontal, self.window)
        self.sliderPosition.setFocusPolicy(Qt.NoFocus)
        self.sliderPosition.setGeometry(155, 170, 700, 30)
        self.sliderPosition.sliderReleased.connect(self.changeSongPosition)
        self.sliderPosition.sliderPressed.connect(self.sliderGrabbed)

        #Song List
        self.songList = QListWidget(self.window)
        self.songList.setGeometry(150, 550, 700, 200)
        self.songList.addItems([filename.split('/')[-1] for filename in self.musicFiles])

        #display everything
        self.window.show()

    def getfile(self):
        filename, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","Music Files (*.mp3 *.ogg *.wav *.opus *.aac)")
        self.musicFiles.append(filename)
        self.songList.addItem(filename.split('/')[-1])
        with open('musicFiles', 'wb') as file:
            pickle.dump(self.musicFiles, file)

    def getFolder(self):
        folder = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
        filenames = glob.glob(folder + "/*.mp3")
        self.musicFiles.extend(filenames)
        self.songList.addItems([filename.split('/')[-1] for filename in filenames])
        with open('musicFiles', 'wb') as file:
            pickle.dump(self.musicFiles, file)

    def play(self, action = None):
        mixer.quit()
        mixer.init(int(self.songFrequency), -16, 2, 512)

        if(self.songList.currentItem() == None):
            self.songList.setCurrentRow(0)
        if(action == "next"):
            if(self.songList.currentRow() == self.songList.count() - 1):
                self.songList.setCurrentRow(0)
            else:
                self.songList.setCurrentRow(self.songList.currentRow() + 1)
        elif(action == "previous"):
            if(self.songList.currentRow() == 0):
                self.songList.setCurrentRow(self.songList.count() - 1)
            else:
                self.songList.setCurrentRow(self.songList.currentRow() - 1)

        selectedSong = [song for song in self.musicFiles if self.songList.currentItem().text() in song][0]
        mixer.music.load(selectedSong)
        mixer.music.play()

        self.currentSongLength = MP3(selectedSong).info.length
        self.sliderPosition.setSliderPosition(0)
        self.labelPosition.setText("0:00:00")
        self.sliderPosition.setRange(0, self.currentSongLength)

        if(not self.timer.isActive()):
            self.timer.start(1000)

    def pause(self):
        if(mixer.get_init()):
            if(not self.paused):
                mixer.music.pause()
                self.paused = True
                self.pauseButton.setText("RESUME")
            else:
                mixer.music.unpause()
                self.paused = False
                self.pauseButton.setText("PAUSE")

    def updateFrequency(self, frequency):
        self.labelFrequencyValue.setText(str(frequency))
        self.songFrequency = str(frequency)

    def updateVolume(self, volume):
        self.labelVolumeValue.setText(str(volume))
        if(self.mixer.get_init()):
            self.mixer.music.set_volume(volume/10)

    def emptyQueue(self):
        self.songList.clear()
        self.musicFiles = []
        with open('musicFiles', 'wb') as file:
            pickle.dump(self.musicFiles, file)

    def updatePositionLabel(self):
        h, m, s = str(self.labelPosition.text()).split(':')
        self.songPositionSecs = int(m)*60 + int(s) + 1
        self.labelPosition.setText(str(datetime.timedelta(seconds=self.songPositionSecs)))
        if(not self.positionSliderGrabbed):
            self.sliderPosition.setSliderPosition(self.songPositionSecs)
        if(self.currentSongLength < self.songPositionSecs):
            self.play('next')


    def changeSongPosition(self):
        self.positionSliderGrabbed = False
        self.songPositionSecs = self.sliderPosition.sliderPosition()
        self.labelPosition.setText(str(datetime.timedelta(seconds=self.songPositionSecs)))
        mixer.music.set_pos(self.songPositionSecs)

    def sliderGrabbed(self):
        self.positionSliderGrabbed = True

    def shuffle(self):
        curItem = self.songList.currentItem()
        random.shuffle(self.musicFiles)
        print(self.songList.currentItem())
        if(curItem != None):
            currentSong = self.songList.currentItem().text()
        self.songList.clear()
        self.songList.addItems([filename.split('/')[-1] for filename in self.musicFiles])

        if(curItem != None):
            items = self.songList.findItems(currentSong,Qt.MatchExactly)
            for item in items:
                self.songList.setCurrentItem(item)

        with open('musicFiles', 'wb') as file:
            pickle.dump(self.musicFiles, file)



app = QApplication(sys.argv)
player = Player()
sys.exit(app.exec_())
